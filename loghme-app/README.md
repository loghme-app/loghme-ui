## To run the project with Docker

After you have run Loghme backend enter:

### `docker build -t loghme-frontend .`

### `docker run -d --name Loghme-frontend -p 3000:3000 --link Loghme-backend:localhost loghme-frontend`