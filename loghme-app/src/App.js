import React, {Component} from 'react';
import './App.css';

import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import Profile from './pages/Profile/profile';
import Footer from './components/Footer/footer';
import HeaderContainer from './components/Header/headerContainer';
import Restaurant from "./pages/Restaurant/restaurant";
import Register from "./pages/Register/register";
import Login from "./pages/Login/login";
import Home from "./pages/Home/home";
import {NotificationContainer} from 'react-notifications';
import 'react-notifications/lib/notifications.css';


class App extends Component {
    render() {
        return <Router>
            <div className="page-container">
                <HeaderContainer/>
                <NotificationContainer/>

                <Switch>
                    <Route exact path="/profile" render={ props => <Profile {...props}/> } />
                    <Route exact path="/register" render={ props => <Register {...props}/> } />
                    <Route exact path="/home" render={ props => <Home {...props}/> } />
                    <Route exact path="/restaurants/:restaurantID" render={ props => <Restaurant {...props}/> } />
                    <Route render={ props => <Login {...props}/> } />
                </Switch>

                <Footer/>
            </div>
        </Router>
    }
}

export default App;
