export function getErrorMsg(error) {
    let msg = "";
    if( error.response && error.response.data.error){
        switch (error.response.data.error) {
            case "FOOD_ITEM_NOT_IN_CART":
                msg = "غذا در سبد خرید وجود ندارد";
                break;
            case "FOOD_NOT_FOUND":
                msg = "غذا وجود ندارد";
                break;
            case "FOOD_OUT_OF_STOCK":
                msg = "موجودی غذا ناکافی است";
                break;
            case "INVALID_CREDIT_AMOUNT":
                msg = "مقدار اعتبار معتبر نیست";
                break;
            case "NO_ITEM_IN_CART":
                msg = "چیزی در سبد خرید نیست";
                break;
            case "NOT_ENOUGH_CREDIT":
                msg = "اعتبار شما کافی نیست";
                break;
            case "ORDER_FROM_DISCOUNT_AND_NORMAL_FOOD":
                msg = "تنها یک نوع سفارش ممکن است";
                break;
            case "ORDER_FROM_MULTIPLE_RESTAURANTS":
                msg = "سفارش تنها از یک رستوران ممکن است";
                break;
            case "ORDER_NOT_FOUND":
                msg = "سفارش موجود نیست";
                break;
            case "RESTAURANT_NOT_FOUND":
                msg = "رستوران موجود نیست";
                break;
            case "RESTAURANT_OUT_OF_REACH":
                msg = "رستوران در محدوده شما نیست";
                break;
            case "USER_NOT_FOUND":
                msg = "شما در لقمه عضو نیستید";
                break;
            case "INVALID_LOGIN_INFO":
                msg = "ایمیل و رمز شما با هم همخوانی ندارند";
                break;
            case "INVALID_GOOGLE_LOGIN_INFO":
                msg = "مشکلی در ورود با گوگل پیش آمده، لطفا دوباره تلاش کنید";
                break;
            case "INVALID_EMAIL_ADDRESS":
                msg = "ایمیل وارد شده معتبر نیست";
                break;
            case "WEAK_PASSWORD":
                msg = "کلمه عبور باید حداقل ۴ کاراکتر باشد";
                break;
            case "EMAIL_ALREADY_EXISTS":
                msg = "شما قبلا با این ایمیل در لقمه عضو شدید";
                break;
            default:
                msg = "خطایی رخ داده است";
        }
    }
    else{
        msg = "خطایی رخ داده است";
    }
    return msg;
}