import React from 'react';
import axios from 'axios';
import {NotificationManager} from 'react-notifications';

import './cart.css'
import {getErrorMsg} from "../../assets/HelperFunctions/getErrorMsg";
import LoadingSpinner from "../LoadingSpinner/loadingSpinner";

function Cart(props) {
    const cartItems = props.cart.items;
    const totalPrice = props.cart.totalPrice;
    const rID = props.cart.restaurantId;
    const foodParty = (props.cart.orderType==="discount");
    const loading = props.loading;

    function finalizeOrder(event, updateCart){
        event.preventDefault();
        axios.post('http://185.166.105.6:30008/orders',null,
            {headers: { Authorization: localStorage.getItem('jwt') }})
            .then((response)=>{
            console.log(response);
            NotificationManager.success(null, 'سفارش ثبت شد.');
            updateCart();
        }).catch((error)=>{
            console.log(error);

            if( error.response && (error.response.data.error === "NO_JWT_TOKEN" || error.response.data.error === "INVALID_JWT_TOKEN")){
                localStorage.removeItem('jwt');
                props.hide();
                props.history.push('/');
            }
            else {
                const error_msg = getErrorMsg(error);
                NotificationManager.error(null, error_msg, 3000);
            }
        })
    }

    function addFood(event, rID, fID, foodParty, updateCart){
        event.preventDefault();
        axios.put(('http://185.166.105.6:30008/cart'), null,
            {
                params:{'restaurantId':rID, 'foodId':fID, 'count':1, 'foodParty':foodParty },
                headers: { Authorization: localStorage.getItem('jwt') }
            })
            .then((response)=>{
            console.log(response);
            updateCart();
        }).catch((error)=>{
            console.log(error);

            if( error.response && (error.response.data.error === "NO_JWT_TOKEN" || error.response.data.error === "INVALID_JWT_TOKEN")){
                localStorage.removeItem('jwt');
                props.hide();
                props.history.push('/');
            }
            else {
                const error_msg = getErrorMsg(error);
                NotificationManager.error(null, error_msg, 3000);
            }
        })
    }

    function deleteFood(event, rID, fID, updateCart){
        event.preventDefault();
        axios.delete(('http://185.166.105.6:30008/cart'),
            {
                params:{ 'restaurantId':rID, 'foodId':fID },
                headers: { Authorization: localStorage.getItem('jwt') }
            })
            .then((response)=>{
            console.log(response);
            updateCart();
        }).catch((error)=>{
            console.log(error);

            if( error.response && (error.response.data.error === "NO_JWT_TOKEN" || error.response.data.error === "INVALID_JWT_TOKEN")){
                localStorage.removeItem('jwt');
                props.hide();
                props.history.push('/');
            }
            else {
                const error_msg = getErrorMsg(error);
                NotificationManager.error(null, error_msg, 3000);
            }
        })
    }

    return(
        <div className="text-center">
            <h6 className="cart-title"> سبد خرید </h6>

            <div className="container-fluid">
            {props.loading ? <LoadingSpinner/> :
                <div>
                    <div className="cart-items-container">
                        <div className="cart-items row center-items">
                            { cartItems.map((item, i)=>(
                                <div key={i} className=" col-12 cart-item">
                                    <div className="row cart-item-row">
                                        <div className="col-7 food-name">
                                            {item.food.name}
                                        </div>
                                        <div className="col-5 food-details">
                                            <div className="add-remove-item">
                                                <a className="not-link" href="#" onClick={(event)=>{deleteFood(event, rID, item.food.id, props.updateCart)}}>
                                                    <span className="flaticon-minus remove-food"></span>
                                                </a>
                                                <span className="food-num">{item.count}</span>
                                                <a className="not-link" href="#" onClick={(event)=>{addFood(event, rID, item.food.id, foodParty, props.updateCart)}}>
                                                    <span className="flaticon-plus add-food"></span>
                                                </a>
                                            </div>
                                            <div className="price"> {item.food.price} تومان</div>
                                        </div>
                                    </div>
                                </div>
                            ))}

                        </div>
                    </div>
                    <div className="final-price">جمع کل: <b>{totalPrice} تومان </b></div>
                    <form onSubmit={(event)=>{finalizeOrder(event, props.updateCart)}}>
                        <input className="submit-order" type="submit" value="تایید نهایی"/>
                    </form>
                </div>
            }
            </div>
        </div>
    );
 }

 export default Cart;