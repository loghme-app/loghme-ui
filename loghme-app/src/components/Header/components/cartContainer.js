import React,{Component} from "react";
import axios from "axios";

import '../header.css';
import Cart from "../../Cart/cart";
import {NotificationManager} from "react-notifications";
import {getErrorMsg} from "../../../assets/HelperFunctions/getErrorMsg";

class CartContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            cart:{items:[]},
            loading:false,
        };
        this.updateCart = this.updateCart.bind(this);
    };

    updateCart(){
        this.setState({loading:true}, ()=> {
            axios.get('http://185.166.105.6:30008/cart',
                {headers: { Authorization: localStorage.getItem('jwt') }})
                .then((response) => {
                console.log(response);
                this.setState({
                    cart: response.data,
                    loading: false,
                });
            }).catch((error) => {
                console.log(error);

                if( error.response && (error.response.data.error === "NO_JWT_TOKEN" || error.response.data.error === "INVALID_JWT_TOKEN")){
                    localStorage.removeItem('jwt');
                    this.props.hide();
                    this.props.history.push('/');
                }
                else {
                    const error_msg = getErrorMsg(error);
                    NotificationManager.error(null, error_msg, 3000);
                }
            })
        });
    }

    componentDidMount() {
        this.updateCart();
    }

    render() {
        return (
            <Cart cart={this.state.cart} updateCart ={this.updateCart} loading={this.state.loading} hide={this.props.hide} {...this.props}/>
        );
    }
}

export default CartContainer;