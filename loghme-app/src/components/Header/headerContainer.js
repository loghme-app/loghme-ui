import {Route, Switch} from "react-router-dom";
import React from "react";
import Header from "./header";


function HeaderContainer(){
    return (
        <Switch>
            <Route exact path="/profile" render={(props) => <Header {...props} type={"profile"}/>}/>
            <Route exact path="/" render={(props) => <Header {...props} type={"login"}/>}/>
            <Route exact path="/register" render={(props) => <Header {...props} type={"register"}/>}/>
            <Route path="/" render={(props) => <Header {...props} type={"general"}/>}/>
        </Switch>
    );
}

export default HeaderContainer;