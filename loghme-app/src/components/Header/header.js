import React, {Component} from 'react';
import {Link} from "react-router-dom";
import { Modal } from 'react-bootstrap';
import CartContainer from "./components/cartContainer";

class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showCart: false
        };

        this.openCart = this.openCart.bind(this);
        this.closeCart = this.closeCart.bind(this);
    }

    openCart(event){
        event.preventDefault();
        this.setState({
           showCart: true
        });
    }

    closeCart(){
        this.setState({
            showCart: false
        });
    }

    logout(){
        if (window.gapi.auth2) {
            const auth2 = window.gapi.auth2.getAuthInstance();
            if (auth2 != null) auth2.signOut().then( ()=> {
                console.log('User signed out.');
                auth2.disconnect();
            }).catch((error) => {
                console.log("Error in google logout");
                console.log(error);
            });
        }
        localStorage.clear();
        sessionStorage.clear();
    }

    render(){
        return (
            <React.Fragment>
                <Modal dialogClassName="basic-modal" show={this.state.showCart} onHide={this.closeCart}>
                    <Modal.Body>
                        <CartContainer {...this.props} hide={this.closeCart}/>
                    </Modal.Body>
                </Modal>

                <header className="header">
                    <div className="container-fluid">
                        <div className="row header-row">
                            <div className="col-1 center-items">
                                <Link to="/home">
                                    <img id="logo" src={require("../../assets/LOGO.png")} alt="Loghme"/>
                                </Link>
                            </div>

                            {this.props.type==="general"
                                ? <div className="col-7 col-lg-8"></div>
                                : (this.props.type==="profile" ?
                                    <div className="col-9"></div> :
                                    <div className="col-10"></div>
                                )
                            }

                            {!((this.props.type==="login")|| (this.props.type==="register")) &&
                                <div className="col-1 cart-button">
                                    <a href="#" onClick={this.openCart} className="cart-ref">
                                        <span className="flaticon-smart-cart"></span>
                                    </a>
                                </div>
                            }


                            {this.props.type==="general" &&
                                <div className="col-2 col-lg-1 profile-button">
                                    <Link to="/profile" className="profile-ref"> حساب کاربری</Link>
                                </div>
                            }

                            {!((this.props.type === "login") || (this.props.type === "register")) &&
                                <div className="col-1 exit-button">
                                    <Link onClick={this.logout} to="/" className="exit-ref">خروج</Link>
                                </div>
                            }

                            {(this.props.type === "login") &&
                                <div className="col-1 ">
                                    <Link to="/register" className="profile-ref">ثبت نام</Link>
                                </div>
                            }

                            {(this.props.type === "register") &&
                            <div className="col-1 ">
                                <Link to="/" className="profile-ref">ورود </Link>
                            </div>
                            }
                        </div>
                    </div>
                </header>
            </React.Fragment>
        );
    }
}

export default Header;