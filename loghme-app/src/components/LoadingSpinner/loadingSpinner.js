import React, {Component} from "react";
import './loadingSpinner.css';
class LoadingSpinner extends Component{
    render(){
        return(
            <div className="row spinner-container justify-content-center">
                <div className="spinner-border text-info" role="status">
                    <span className="sr-only">Loading...</span>
                </div>
            </div>
        );
    }
}

export default LoadingSpinner;