import React, {Component} from "react";
import axios from "axios";


class Timer extends Component {
    constructor(props) {
        super(props);

        this.state = {
            secondsRemaining: 0
        };

        this.updateTime = this.updateTime.bind(this);
        this.fetchRemainingTime = this.fetchRemainingTime.bind(this);
    }

    render(){
        const time = this.secondsToTime(this.state.secondsRemaining)
        return (
            <span>
                {this.twoDigits(time.minutes)}:{this.twoDigits(time.seconds)}
            </span>
        );
    }

    secondsToTime(secs){
        let divisor_for_minutes = secs % (60 * 60);
        let minutes = Math.floor(divisor_for_minutes / 60);

        let divisor_for_seconds = divisor_for_minutes % 60;
        let seconds = Math.ceil(divisor_for_seconds);

        return {
            "minutes": minutes,
            "seconds": seconds
        };
    }

    twoDigits(number){
        return (number>9)?(number.toString()):("0"+number.toString())
    }

    componentDidMount() {
        this.fetchRemainingTime();
    }

    componentWillUnmount() {
        clearInterval(this.timerId);
    }

    fetchRemainingTime(){
        clearInterval(this.timerId);
        axios.get(`http://185.166.105.6:30008/discountFoods/time`,
            {headers:{ Authorization: localStorage.getItem('jwt') }})
            .then((response)=> {
                this.setState({
                    secondsRemaining: response.data.remainingTime,
                });
                this.timerId = setInterval(
                    this.updateTime
                    , 1000
                );
            })
            .catch((error)=> {
                console.log(error);

                if( error.response && (error.response.data.error === "NO_JWT_TOKEN" || error.response.data.error === "INVALID_JWT_TOKEN")){
                    localStorage.removeItem('jwt');
                    this.props.history.push('/');
                }
            })
    }

    updateTime(){
        if(this.state.secondsRemaining === 0){
            this.fetchRemainingTime();
            this.props.onFinish();
        }
        if(this.state.secondsRemaining > 0){
            this.setState((prevState) => ({
                secondsRemaining: prevState.secondsRemaining - 1
            }));
        }
    }
}

export default Timer;