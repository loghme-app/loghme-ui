import React from 'react';
import './footer.css';

function Footer(){
    return(
        <footer className="footer">
            <span className="center-items">
                © تمامی حقوق متعلق به لقمه است.
            </span>
        </footer>
    );
}

export default Footer;