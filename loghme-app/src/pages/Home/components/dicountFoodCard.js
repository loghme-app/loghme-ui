import React, {Component} from "react";

class DiscountFoodCard extends Component {
    constructor(props) {
        super(props);
    }

    render(){
        return (
            <div className="card">
                <div className="row discount-food-top">
                    <div className="col-6 medium-padding-right">
                        <img src={this.props.food.image} className="discount-food-image" alt="food_img"/> <br/>
                    </div>
                    <div className="col-6 no-padding-right right-align">
                        <div className="col-12 small-padding">
                            <span className="discount-food-title"> {this.props.food.name} </span>
                            <br/>
                            <span className="discount-food-subtitle"> {this.props.food.popularity} </span>
                            <span className="star discount-food-subtitle">&#9733;</span>
                        </div>
                    </div>
                </div>
                <div className="row discount-food-price-section center-items">
                    <span className="light-text discount-food-subtitle" id="oldPrice" >  {this.props.food.oldPrice}  </span>
                    <span className="light-text discount-food-subtitle"> {this.props.food.price}  تومان</span> <br/>
                </div>
                <div className="row discount-food-buy-section center-items custom-border">
                    <div className="col-6 center-items no-padding-right">
                        <span className="remained-count buy-section-btn-width text-center">موجودی: {this.props.food.count}‌</span>
                    </div>
                    <div className="col-6 center-items no-padding-right">
                        <form className="buy-section-btn-width center-items" onSubmit={this.props.clickOnBy}>
                            <input className="modal-add-cart buy-section-btn-width text-center" type="submit" value="خرید"/>
                        </form>
                    </div>
                </div>
                <div className="row discount-food-restaurant-section center-items">
                    <span className="light-text restaurant-title">  {this.props.food.restaurantName}  </span>
                </div>
            </div>
        );
    }
}

export default DiscountFoodCard;