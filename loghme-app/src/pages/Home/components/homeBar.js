import React,{Component} from "react";

class HomeBar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            foodName:"",
            restaurantName:"",
        };

        this.handleFoodNameChange = this.handleFoodNameChange.bind(this);
        this.handleRestaurantNameChange = this.handleRestaurantNameChange.bind(this);
    }

    handleFoodNameChange(event){
        this.setState({
           foodName:event.target.value,
        });
    }

    handleRestaurantNameChange(event){
        this.setState({
            restaurantName:event.target.value,
        });
    }

    render(){
        return(
            <div className="home-head-bar ">
                <div className="row">
                    <img className="loghme-img justify-content-center align-content-center" src={require("../../../assets/LOGO.png")} alt="Loghme"/>
                    <div className="col-12 home-text text-center">اولین و بزرگترین وب‌ سایت سفارش آنلاین غذا در دانشگاه تهران</div>
                    <div className="col-7 search-bar ">
                    <div className="row filter-inputs">

                        <div className="col-4">
                            <input type="text" className="form-control search-input-filter"
                                   placeholder="نام غذا" value={this.state.foodName} onChange={this.handleFoodNameChange}/>
                        </div>
                        <div className="col-4">
                            <input type="text" className="form-control search-input-filter"
                                   placeholder="نام رستوران" value={this.state.restaurantName} onChange={this.handleRestaurantNameChange}/>
                        </div>
                        <div className="col-4 text-center">
                            <input type="submit" value='جست و جو'  id="search-btn" className="home-page-btn"
                                   onClick={() => this.props.searchRestaurants(
                                       this.state.restaurantName, this.state.foodName)} />
                        </div>

                    </div>

                    </div>
                </div>
            </div>

        );
    }
}

export default HomeBar;