import React,{Component} from "react";
import {Link} from "react-router-dom";
import LoadingSpinner from "../../../components/LoadingSpinner/loadingSpinner";
import '../home.css';

class RestaurantsList extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render(){
        const restaurants = this.props.restaurants;
        return(
            <div className="restaurants-container container">
                <div className="row">
                        <div className="make-center navy-underlined-title home-section-title">رستوران‌ها</div>
                </div>

                {this.props.loading ? <LoadingSpinner/> :
                    <div className="row restaurants-list">
                        {restaurants.map(item => (
                            <div key={item.id} className="col-3 ">
                                <div className="restaurant-container text-center">
                                    <div className="restaurant-image-section">
                                        <img src={item.logo} className="restaurant-image" alt="food_img"/> <br/>
                                    </div>

                                    <div className="restaurant-name-section">
                                        <span className="bold-text restaurant-name">{item.name}</span>
                                    </div>

                                    <div className="container restaurant-btn-section">
                                        <Link to={`/restaurants/${item.id}`}>
                                            <input type="submit" value="نمایش منو"
                                                   className="home-page-btn menu-btn"/>
                                        </Link>
                                    </div>
                                </div>
                            </div>
                        ))}
                    </div>
                }
                <div className="row center-items">
                    <div className="col-4 text-center">
                        <input type="submit" value='نمایش بیشتر ...' className="load-more-btn" onClick={this.props.getRestaurants} />
                    </div>
                </div>
            </div>
        );
    }
}

export default RestaurantsList;