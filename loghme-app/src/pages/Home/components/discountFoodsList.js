import React, {Component} from "react";
import {Modal} from "react-bootstrap";
import DiscountFood from "./discountFood";
import DiscountFoodCard from "./dicountFoodCard";
import '../home.css';

class DiscountFoodsList extends Component {
    constructor(props) {
        super(props);
        this.state ={
            showFoodDetails: false,
            foodId: 0,
        };
        this.closeDiscountFood = this.closeDiscountFood.bind(this);
        console.log(this.props.foods);
    }

    openDiscountFood(foodId, event){
        event.preventDefault();
        this.setState({
           showFoodDetails: true,
           foodId: foodId
        });
    }

    closeDiscountFood(){
        this.setState({
            showFoodDetails: false,
            foodId: 0
        });
    }

    render(){
        return(
            <div className="col-12 cards-wrapper">
                <Modal dialogClassName="discountFood-modal" show={this.state.showFoodDetails} onHide={this.closeDiscountFood}>
                    <Modal.Body >
                        <DiscountFood fId={this.state.foodId} {...this.props}/>
                    </Modal.Body>
                </Modal>

                <div className="cards">
                    {this.props.foods.map( food => {
                        return <DiscountFoodCard food={food} clickOnBy={this.openDiscountFood.bind(this, food.id)}/>;
                    })}
                </div>
            </div>
        );
    }
}

export default DiscountFoodsList;