import React,{Component} from "react";
import axios from 'axios';
import DiscountFoodsList from "./discountFoodsList";
import {NotificationManager} from "react-notifications";
import {getErrorMsg} from "../../../assets/HelperFunctions/getErrorMsg";
import Timer from "../../../components/Timer/timer";

class FoodPartyContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            foods: [],
            remainingTime: 0
        }
    }

    render(){
        return (
            <div className="container-fluid food-party-container">
                <div className="row">
                    <div className="col-12 center-items">
                        <div className="navy-underlined-title home-section-title text-center">
                            جشن غذا!
                        </div>
                    </div>
                    <div className="col-12 center-items">
                        <div className="food-party-timer text-center">
                            زمان باقی مانده:
                            <span>      </span>
                            <Timer onFinish={this.fetchFoodParty.bind(this)} {...this.props} />
                        </div>
                    </div>
                    <DiscountFoodsList foods={this.state.foods} {...this.props}/>
                </div>
            </div>
        );
    }

    componentDidMount() {
        this.fetchFoodParty();
    }

    fetchFoodParty(){
        axios.get(`http://185.166.105.6:30008/discountFoods`,
            {headers:{ Authorization: localStorage.getItem('jwt') }})
            .then((response)=> {
                console.log(response);
                this.setState({
                    foods: response.data,
                });
            })
            .catch((error)=> {
                console.log(error);

                if( error.response && (error.response.data.error === "NO_JWT_TOKEN" || error.response.data.error === "INVALID_JWT_TOKEN")){
                    localStorage.removeItem('jwt');
                    this.props.history.push('/');
                }
                else {
                    const error_msg = getErrorMsg(error);
                    NotificationManager.error(null, error_msg, 3000);
                }
        })
    }
}

export default FoodPartyContainer;