import React,{Component} from "react";
import axios from 'axios';

import './home.css';
import HomeBar from "./components/homeBar";
import Food from "../Restaurant/components/Food";
import FoodPartyContainer from "./components/foodPartyContainer";
import RestaurantsList from "./components/restaurantsList";
import {NotificationManager} from "react-notifications";
import {getErrorMsg} from "../../assets/HelperFunctions/getErrorMsg";

class Home extends Component{
    constructor(props) {
        super(props);
        this.state= {
            loading: false,
            restaurants:[],
            pageNum:1,
        };
        this.itemsPerPage = 16;
        this.getRestaurants = this.getRestaurants.bind(this);
        this.searchRestaurants = this.searchRestaurants.bind(this);
    }

    getRestaurants(){
        this.setState({loading:true}, ()=>{
            axios.get('http://185.166.105.6:30008/restaurants', {params:{
                page_num : this.state.pageNum,
                items_per_page : this.itemsPerPage,
                }, headers:{ Authorization: localStorage.getItem('jwt') }
                })
                .then((response)=> {
                this.setState(prevState => ({
                    restaurants: [...prevState.restaurants, ...response.data],
                    pageNum: prevState.pageNum+1,
                    loading: false,
                }));})
                .catch((error)=> {
                console.log(error);

                if( error.response && (error.response.data.error === "NO_JWT_TOKEN" || error.response.data.error === "INVALID_JWT_TOKEN")){
                    localStorage.removeItem('jwt');
                    this.props.history.push('/');
                }
                else {
                    const error_msg = getErrorMsg(error);
                    NotificationManager.error(null, error_msg, 3000);
                }
            })
        })
    }

    searchRestaurants(restaurantKeyword, foodKeyword){
        this.setState({loading:true}, ()=>{
            axios.get('http://185.166.105.6:30008/search',
            {params:{'restaurant_keyword':restaurantKeyword, 'food_keyword':foodKeyword},
                    headers:{ Authorization: localStorage.getItem('jwt') }})
                .then((response)=> {
                this.setState({
                    restaurants: response.data,
                    loading: false,
                });
            }).catch((error)=> {
                console.log(error);

                if( error.response && (error.response.data.error === "NO_JWT_TOKEN" || error.response.data.error === "INVALID_JWT_TOKEN")){
                    localStorage.removeItem('jwt');
                    this.props.history.push('/');
                }
                else {
                    const error_msg = getErrorMsg(error);
                    NotificationManager.error(null, error_msg, 3000);
                }
            })
        })
    }

    componentDidMount() {
        this.getRestaurants();
    }

    render() {
        return (
            <div className="content-wrap container-fluid">
                <HomeBar searchRestaurants={this.searchRestaurants}/>
                <FoodPartyContainer {...this.props}/>
                <RestaurantsList restaurants={this.state.restaurants} loading={this.state.loading} getRestaurants={this.getRestaurants}/>
            </div>
        );
    }
}

export default Home;