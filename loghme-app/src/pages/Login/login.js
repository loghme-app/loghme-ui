import React, {Component} from "react";

import './login.css';
import {Link} from "react-router-dom";
import axios from 'axios';
import {NotificationManager} from "react-notifications";
import {getErrorMsg} from "../../assets/HelperFunctions/getErrorMsg";
import { GoogleLogin } from 'react-google-login';

const CLIENT_ID = "867238827080-d0eljctvi3lgjoen339p5njp9h8t9tv8.apps.googleusercontent.com";

class Login extends Component{
    constructor(props) {
        super(props);
        this.state = {
            email:"",
            password:"",
            emailIsValid: false,
            passwordIsValid: false,
        };

        this.handleEmailChange = this.handleEmailChange.bind(this);
        this.handlePasswordChange = this.handlePasswordChange.bind(this);
        this.emailIsValid = this.emailIsValid.bind(this);
        this.submitLoginInfo = this.submitLoginInfo.bind(this);
        this.onSignIn = this.onSignIn.bind(this);
    }

    componentDidMount() {
        this.props.history.push('/');
        this.renderGoogleSigninButton();
        axios.get('http://185.166.105.6:30008/token',
            {headers: { Authorization: localStorage.getItem('jwt') }})
            .then((response) => {
                this.props.history.push('/home');
            }).catch((error) => {
            console.log(error);
        })
    }

    renderGoogleSigninButton(){
        if(window.gapi && window.gapi.signin2){
            window.gapi.signin2.render('google-signin', {
                'onsuccess': this.onSignIn
            });
        }
    }

    onSignIn(googleUser){
        // const profile = googleUser.getBasicProfile();
        const id_token = googleUser.getAuthResponse().id_token;
        googleUser.disconnect();
        axios.post('http://185.166.105.6:30008/login/google',
            null,
            {params: {'google_token':id_token}})
            .then((response) => {
                localStorage.setItem('jwt', response.data.jwt);
                this.props.history.push('/home');
            })
            .catch((error) => {
                console.log(error);
                if(error.response && error.response.data.error === "USER_NOT_FOUND"){
                    this.props.history.push('/register');
                }
                const error_msg = getErrorMsg(error);
                NotificationManager.error(null, error_msg, 3000);
            })
    }

    onSignInFailure(){
        NotificationManager.error(null, "اطلاعات وارد شده درست نیست، دوباره تلاش کنید", 3000);
    }

    handleEmailChange(event){
        this.setState({
            email: event.target.value,
            emailIsValid: this.emailIsValid(event.target.value)
        });
    }

    emailIsValid(email){
        return /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email);
    }

    handlePasswordChange(event){
        this.setState({
           password: event.target.value,
           passwordIsValid: (event.target.value.length >= 4)
        });
    }

    submitLoginInfo(event){
        event.preventDefault();
        axios.post('http://185.166.105.6:30008/login',
            null,
            {params: {'email':this.state.email, 'password':this.state.password}})
            .then((response) => {
               localStorage.setItem('jwt', response.data.jwt);
               this.props.history.push('/home');
            })
            .catch((error) => {
                const error_msg = getErrorMsg(error);
                NotificationManager.error(null, error_msg, 3000);
            })
    }

    render() {
        return (
            <div className="content-wrap container-fluid">
                <div className="simple-head-bar row">
                    <span id="welcome-txt" className="bold-text"> به لقمه خوش آمدید!</span>
                </div>

                <div className="container-fluid login-form-container">
                    <div className="row center-items">
                        <form className="col-6 login-form" onSubmit={this.submitLoginInfo}>
                            <div className="row justify-content-center align-items-center">
                                <div className="col-12 form-group row">
                                    <label htmlFor="email" className="col-4">ایمیل:</label>
                                    <input type="text" className={"col-8 form-control field " + (this.state.emailIsValid?"":"is-invalid")}
                                           id="email" onChange={this.handleEmailChange}/>
                                </div>
                                <div className="col-12 form-group row">
                                    <label htmlFor="password" className="col-4">رمز:</label>
                                    <input type="password" className={"col-8 form-control field " + (this.state.passwordIsValid?"":"is-invalid")}
                                           id="password" onChange={this.handlePasswordChange}/>
                                </div>
                                <div className="col-8"/>
                                <div className="col-2 center-items">
                                    <GoogleLogin
                                        clientId={CLIENT_ID}
                                        buttonText="ورود با Google"
                                        onSuccess={this.onSignIn}
                                        className="google-login-btn center-vertical center-items"
                                    />
                                </div>
                                <div className="col-2 center-items">
                                    <input type="submit" disabled={!this.state.emailIsValid || !this.state.passwordIsValid}
                                           value="ورود" className={
                                               (!this.state.emailIsValid || !this.state.passwordIsValid)?"disabled-login-btn":"enabled-login-btn"}/>
                                </div>

                                <div className="col-12 register-ref">
                                    <Link to="/register" className="ref align-right ">عضو لقمه نیستم.</Link>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default Login;

