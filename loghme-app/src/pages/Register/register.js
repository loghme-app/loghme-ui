import React, {Component} from "react";

import './register.css';
import axios from "axios";
import {getErrorMsg} from "../../assets/HelperFunctions/getErrorMsg";
import {NotificationManager} from "react-notifications";

class Register extends Component{
    constructor(props) {
        super(props);
        this.state = {
            firstName:"",
            lastName:"",
            phone:"",
            email:"",
            password:"",
            isFirstNameValid: false,
            isLastNameValid: false,
            isPhoneValid: false,
            isEmailValid: false,
            isPasswordValid: false
        };

        this.handleFirstNameChange = this.handleFirstNameChange.bind(this);
        this.handleLastNameChange = this.handleLastNameChange.bind(this);
        this.handlePhoneChange = this.handlePhoneChange.bind(this);
        this.handleEmailChange = this.handleEmailChange.bind(this);
        this.handlePasswordChange = this.handlePasswordChange.bind(this);
        this.phoneIsValid = this.phoneIsValid.bind(this);
        this.emailIsValid = this.emailIsValid.bind(this);
        this.onlyNumbers = this.onlyNumbers.bind(this);
        this.submitRegisterInfo = this.submitRegisterInfo.bind(this);
    }

    componentDidMount() {
        axios.get('http://185.166.105.6:30008/token',
            {headers: { Authorization: localStorage.getItem('jwt') }})
            .then((response) => {
                this.props.history.push('/home');
            }).catch((error) => {
            console.log(error);
        })
    }

    render() {
        const validInputs = this.state.isFirstNameValid && this.state.isLastNameValid &&
            this.state.isPhoneValid && this.state.isEmailValid && this.state.isPasswordValid;
        return (
            <div className="content-wrap container-fluid">
                <div className="simple-head-bar row">
                    <span id="welcome-txt" className="bold-text"> به لقمه خوش آمدید!</span>
                </div>

                <div className="container-fluid register-form-container">
                    <div className="row center-items">
                        <form className="col-6 register-form" onSubmit={this.submitRegisterInfo}>
                            <div className="row justify-content-center align-items-center">
                                <div className="col-12 form-group row">
                                    <label htmlFor="firstname" className="col-4">نام:</label>
                                    <input type="text" className={"col-8 form-control field " + (this.state.isFirstNameValid?"":"is-invalid")}
                                           id="firstname" onChange={this.handleFirstNameChange}/>
                                </div>
                                <div className="col-12 form-group row">
                                    <label htmlFor="lastname" className="col-4">نام خانوادگی:</label>
                                    <input type="text" className={"col-8 form-control field " + (this.state.isLastNameValid?"":"is-invalid")}
                                           id="lastname" onChange={this.handleLastNameChange}/>
                                </div>
                                <div className="col-12 form-group row">
                                    <label htmlFor="phone" className="col-4">شماره تلفن همراه:</label>
                                    <input type="text" className={"col-8 form-control field " + (this.state.isPhoneValid?"":"is-invalid")}
                                           id="phone" onChange={this.handlePhoneChange} value={this.state.phone}/>
                                </div>
                                <div className="col-12 form-group row">
                                    <label htmlFor="email" className="col-4">ایمیل:</label>
                                    <input type="text" className={"col-8 form-control field " + (this.state.isEmailValid?"":"is-invalid")}
                                           id="email" onChange={this.handleEmailChange}/>
                                </div>
                                <div className="col-12 form-group row">
                                    <label htmlFor="password" className="col-4">رمز:</label>
                                    <input type="password" className={"col-8 form-control field " + (this.state.isPasswordValid?"":"is-invalid")}
                                           id="password" onChange={this.handlePasswordChange}/>
                                </div>
                                <div className="col-12">
                                    <input type="submit" value="ثبت نام" className={validInputs?"enabled-register-btn":"disabled-register-btn"}/>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        );
    }

    handleFirstNameChange(event){
        this.setState({
            firstName: event.target.value,
            isFirstNameValid: event.target.value !== ""
        });
    }

    handleLastNameChange(event){
        this.setState({
            lastName: event.target.value,
            isLastNameValid: event.target.value !== ""
        });
    }

    handlePhoneChange(event){
        if(this.onlyNumbers(event.target.value)){
            this.setState({
                phone: event.target.value,
                isPhoneValid: this.phoneIsValid(event.target.value)
            });
        }
    }

    handleEmailChange(event){
        this.setState({
            email: event.target.value,
            isEmailValid: this.emailIsValid(event.target.value)
        });
    }

    handlePasswordChange(event){
        this.setState({
            password: event.target.value,
            isPasswordValid: event.target.value.length >= 4
        });
    }

    phoneIsValid(phone){
        return /^[0][9]\d{9}$/.test(phone);
    }

    onlyNumbers(phone){
        return /^[0-9]*$/.test(phone);
    }

    emailIsValid(email){
        return /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email);
    }

    submitRegisterInfo(event){
        event.preventDefault();
        axios.post('http://185.166.105.6:30008/register',
            null,
            {params: {'first_name':this.state.firstName, 'last_name':this.state.lastName,'email':this.state.email, 'password':this.state.password, 'phone':this.state.phone}})
            .then((response) => {
                NotificationManager.success(null, 'شما با موفقیت عضو لقمه شدید.');
                this.props.history.push('/');
            })
            .catch((error) => {
                console.log(error);
                const error_msg = getErrorMsg(error);
                NotificationManager.error(null, error_msg, 3000);
            })
    }
}

export default Register;