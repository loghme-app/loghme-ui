import React, {Component} from 'react';
import axios from 'axios';
import './profile.css';
import ProfileFormContainer from "./components/profileFormContainer";
import InfoBar from "./components/infoBar";
import LoadingSpinner from "../../components/LoadingSpinner/loadingSpinner";
import {NotificationManager} from "react-notifications";
import {getErrorMsg} from "../../assets/HelperFunctions/getErrorMsg";


class Profile extends Component {
    constructor(props) {
        super(props);
        this.state={
            user:{},
            loading:false,
        };

        this.fetchUser = this.fetchUser.bind(this);
    }

    fetchUser(){
        this.setState({loading:true},()=>{
            axios.get('http://185.166.105.6:30008/user', {
                responseType: 'json',
                headers: { Authorization: localStorage.getItem('jwt') }
            })
                .then((response) => {
                this.setState({
                    loading: false,
                    user: response.data,
                });
            }).catch((error) => {
                console.log(error);

                if( error.response && (error.response.data.error === "NO_JWT_TOKEN" || error.response.data.error === "INVALID_JWT_TOKEN")){
                    localStorage.removeItem('jwt');
                    this.props.history.push('/');
                }
                else{
                    const error_msg = getErrorMsg(error);
                    NotificationManager.error(null, error_msg, 3000);
                }
            })
        });

    }

    componentDidMount() {
        this.fetchUser();
    }

    render() {
        return (
            <div className="content-wrap container-fluid">
                {this.state.loading ? <LoadingSpinner/> :
                    <div>
                        <InfoBar user={this.state.user} {...this.props}/>
                        <ProfileFormContainer onCreditIncrease={this.fetchUser} {...this.props}/>
                    </div>
                }

            </div>
        );
    }
}

export default Profile;