import React, {Component} from 'react';

import ProfileForm from "./profileForm";

class ProfileFormContainer extends Component{
    constructor(props) {
        super(props);

        this.state = {
            selected: "orders",
        };

        this.clickOrders = this.clickOrders.bind(this);
        this.clickCredit = this.clickCredit.bind(this);
    }

    clickOrders(event){
        event.preventDefault();
        this.setState({
            selected: "orders",
        })
    }

    clickCredit(event){
        event.preventDefault();
        this.setState({
            selected: "credit",
        })
    }

    render(){
        return(
            <div className="container-fluid profile-form-container">
                <div className="row justify-content-center align-items-center tab-choices">
                    <div className="col-8">
                        <ul className="nav nav-fill row">
                            <li className="col-6 right-tab nav-item nav-link" id={(this.state.selected==="orders"?"":"non-")+"active-tab"}>
                                <a id={(this.state.selected==="orders"?"":"non-")+"active-tab-ref"} href="#" onClick={this.clickOrders}>سفارش‌ها</a>
                            </li>
                            <li className="col-6 left-tab nav-item nav-link" id={(this.state.selected==="credit"?"":"non-")+"active-tab"}>
                                <a id={(this.state.selected==="credit"?"":"non-")+"active-tab-ref"} href="#" onClick={this.clickCredit}>افزایش اعتبار</a>
                            </li>
                        </ul>
                    </div>
                </div>

                <ProfileForm content={this.state.selected} {...this.props}/>
            </div>
        );
    }
}

export default ProfileFormContainer;