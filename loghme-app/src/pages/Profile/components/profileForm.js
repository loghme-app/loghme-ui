import React, {Component} from 'react';

import CreditCharger from "./creditCharger";
import OrdersList from "./ordersList";

class ProfileForm extends Component {
    constructor(props) {
        super(props);
    }

    render(){
        return ((this.props.content === "credit")
            ? <CreditCharger updateCredit={this.props.onCreditIncrease} {...this.props} />
            : <OrdersList {...this.props}/>
        );
    }
}

export default ProfileForm;