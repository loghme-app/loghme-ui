import React from 'react';


function InfoBar(props){
    return (
        <div className="simple-head-bar row">
            <div className="col-7 user-name-section">
                        <span className="flaticon-account user-icon">
                        </span>
                <span className="user-name">
                    {props.user.firstName} {props.user.lastName}
                </span>
            </div>
            <div className="col-5 user-details-section">
                <div className="user-detail">
                    <span className="flaticon-phone"></span>
                    <span> {props.user.telephoneNumber} </span>
                </div>
                <div className="user-detail">
                    <span className="flaticon-mail"></span>
                    <span> {props.user.email} </span>
                </div>
                <div className="user-detail">
                    <span className="flaticon-card"></span>
                    <span> {props.user.credit} تومان </span>
                </div>
            </div>
        </div>
    );
}

export default InfoBar;