import React, {Component} from 'react';
import axios from "axios";
import { Modal } from 'react-bootstrap';
import Order from "./order";
import LoadingSpinner from "../../../components/LoadingSpinner/loadingSpinner";
import {NotificationManager} from "react-notifications";
import {getErrorMsg} from "../../../assets/HelperFunctions/getErrorMsg";


class OrdersList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            orders: [],
            showOrderDetails: false,
            orderId: 0,
            loading: false,
        };

        this.closeOrder = this.closeOrder.bind(this);
        this.fetchOrders = this.fetchOrders.bind(this);
        this.updateOrders = this.updateOrders.bind(this);
    }

    componentDidMount() {
        this.fetchOrders();
        this.timerId = setInterval(
            this.updateOrders
            , 5000
        );
    }

    componentWillUnmount() {
        clearInterval(this.timerId);
    }

    fetchOrders(){
        this.setState({loading:true}, ()=>{
            axios.get('http://185.166.105.6:30008/orders', {
                responseType: 'json',
                headers: { Authorization: localStorage.getItem('jwt') }
            })
                .then((response) => {
                console.log(response);
                this.setState({
                    orders: response.data,
                    loading: false,
                });
            }).catch((error) => {
                console.log(error);

                if( error.response && (error.response.data.error === "NO_JWT_TOKEN" || error.response.data.error === "INVALID_JWT_TOKEN")){
                    localStorage.removeItem('jwt');
                    this.props.history.push('/');
                }
                else {
                    const error_msg = getErrorMsg(error);
                    NotificationManager.error(null, error_msg, 3000);
                }
            })
        });
    }

    updateOrders(){
        axios.get('http://185.166.105.6:30008/orders', {
            responseType: 'json',
            headers: { Authorization: localStorage.getItem('jwt') }
        })
            .then((response) => {
            this.setState({
                orders: response.data,
            });
        }).catch((error) => {
            console.log(error);

            if( error.response && (error.response.data.error === "NO_JWT_TOKEN" || error.response.data.error === "INVALID_JWT_TOKEN")){
                localStorage.removeItem('jwt');
                this.props.history.push('/');
            }
        })
    }

    openOrder(orderId, event){
        event.preventDefault();
        this.setState({
            showOrderDetails: true,
            orderId: orderId
        });
    }

    closeOrder(){
        this.setState({
            showOrderDetails: false,
            orderId: 0
        });
    }

    render() {
        const { orders } = this.state;
        return (
            <div className="container-fluid">
                {this.state.loading ? <LoadingSpinner/> :
                    <div className="orders-list center-items">
                        <Modal dialogClassName="basic-modal" show={this.state.showOrderDetails}
                               onHide={this.closeOrder}>
                            <Modal.Body>
                                <Order id={this.state.orderId} {...this.props}/>
                            </Modal.Body>
                        </Modal>
                        <div className="container text-center orders-container">

                            {orders.map((item,id) => (
                                <div className="list-group list-group-horizontal align-ltr order">
                                    <div className=" col-sm-1 order-item center-vertical number-size">{id+1}</div>
                                    <div className="col-sm-7 order-item center-vertical">{item.restaurantName}</div>
                                    <div className=" col-sm-4 order-item center-vertical">
                                        {(item.status === 'delivering') &&
                                        <a href="#" onClick={this.openOrder.bind(this, item.id)}
                                           className="disabled status-delivery not-link">پیک در مسیر</a>
                                        }
                                        {(item.status === 'findingDelivery') &&
                                        <a href="#" onClick={this.openOrder.bind(this, item.id)}
                                           className="disabled status-delivery-search not-link">در جست و جوی پیک</a>
                                        }
                                        {(item.status === 'delivered') &&
                                        <a href="#" onClick={this.openOrder.bind(this, item.id)}
                                           className="status-delivered not-link">مشاهده فاکتور</a>
                                        }
                                    </div>
                                </div>
                            ))}
                        </div>
                    </div>
                }
            </div>
        );
    }
}

export default OrdersList;