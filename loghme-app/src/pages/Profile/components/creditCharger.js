import React, {Component} from "react";
import axios from 'axios';
import {NotificationManager} from "react-notifications";
import {getErrorMsg} from "../../../assets/HelperFunctions/getErrorMsg";

class CreditCharger extends Component {
    constructor(props) {
        super(props);
        this.state = {
            amount: "",
        };

        this.handleCreditChange = this.handleCreditChange.bind(this);
        this.amountIsValid = this.amountIsValid.bind(this);
        this.submitCharge = this.submitCharge.bind(this);
    }

    amountIsValid(amount){
        return /^[1-9][0-9]*$/.test(amount) || amount === "";
    }

    handleCreditChange(event){
        if(this.amountIsValid(event.target.value)){
            this.setState({
               amount: event.target.value,
            });
        }
    }

    submitCharge(event){
        event.preventDefault();
        axios.put('http://185.166.105.6:30008/credit',
            null,
            {params: {'amount':this.state.amount},
                headers:{ Authorization: localStorage.getItem('jwt') }})
            .then((response) => {
                this.setState({
                    amount: ""
                });
                this.props.updateCredit();
            })
            .catch((error) => {
                console.log(error);

                if( error.response && (error.response.data.error === "NO_JWT_TOKEN" || error.response.data.error === "INVALID_JWT_TOKEN")){
                    localStorage.removeItem('jwt');
                    this.props.history.push('/');
                }
                else {
                    const error_msg = getErrorMsg(error);
                    NotificationManager.error(null, error_msg, 3000);
                }
            })
    }

    render(){
        return <div className="row justify-content-center align-items-center">
            <div className="col-12">
                <form className="profile-form" onSubmit={this.submitCharge}>
                    <div className="row ">
                        <div className="col-8">
                            <input type="text" className="form-control credit-amount" id="credit-amount"
                                   placeholder="میزان افزایش اعتبار" value={this.state.amount} onChange={this.handleCreditChange}/>
                        </div>
                        <div className="col-4">
                            <input type="submit" value='افزایش' disabled={this.state.amount===""} id="increase-credit-btn"
                                    className={(this.state.amount==="") ? "disabled-credit-btn" : "enabled-credit-btn"} />
                        </div>
                    </div>
                </form>
            </div>
        </div>;
    }
}

export default CreditCharger;

