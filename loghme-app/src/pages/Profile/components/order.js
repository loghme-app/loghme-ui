import React, {Component} from 'react';
import axios from "axios";
import {NotificationManager} from "react-notifications";
import {getErrorMsg} from "../../../assets/HelperFunctions/getErrorMsg";

class Order extends Component {
    constructor(props) {
        super(props);
        this.state = {
            order: {
                foods: []
            }
        };
    }

    componentDidMount() {
        axios.get('http://185.166.105.6:30008/orders/' + this.props.id, {
            responseType: 'json',
            headers: { Authorization: localStorage.getItem('jwt') }
        })
            .then((response) => {
                console.log("fetching single order for modal:")
                console.log(response);
                this.setState({
                    order: response.data
                });
            })
            .catch((error) => {
                console.log(error);

                if( error.response && (error.response.data.error === "NO_JWT_TOKEN" || error.response.data.error === "INVALID_JWT_TOKEN")){
                    localStorage.removeItem('jwt');
                    this.props.history.push('/');
                }
                else {
                    const error_msg = getErrorMsg(error);
                    NotificationManager.error(null, error_msg, 3000);
                }
            })
    }

    render(){
        return (
            <React.Fragment>
                <h6 className="order-details-title"> {this.state.order.restaurantName} </h6>
                <table className="table table-bordered order-table">
                    <thead className="thead-light order-table-header">
                    <tr>
                        <th className="order-table-id-col">ردیف</th>
                        <th className="order-table-food-col">نام غذا</th>
                        <th className="order-table-count-col">تعداد</th>
                        <th className="order-table-price-col">قیمت</th>
                    </tr>
                    </thead>
                    <tbody>

                    {this.state.order.foods.map((item, rowNumber) => (
                        <tr>
                            <td>{rowNumber}</td>
                            <td>{item.food.name}</td>
                            <td>{item.count}</td>
                            <td>{item.food.price}</td>
                        </tr>
                    ))}

                    </tbody>
                </table>
                <div className="final-price">
                    <b>جمع کل: {this.state.order.price} تومان </b>
                </div>
            </React.Fragment>
        );
    }
}

export default Order;