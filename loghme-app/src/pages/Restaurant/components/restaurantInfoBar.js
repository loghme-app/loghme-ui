import React from 'react';

function RestaurantInfoBar(props) {
    return(
        <div className="flex-column align-items-center" id="restaurant-info">
            <img src={props.restaurantLogo} id="restaurant-logo" alt="Restaurant_img"/>
            <span className="bold-text" id="restaurant-title">{props.restaurantName}</span>
        </div>
    );
}

export default RestaurantInfoBar;
