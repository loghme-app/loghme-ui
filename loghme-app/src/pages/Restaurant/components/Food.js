import React, {Component} from "react";
import axios from 'axios';

import '../restaurant.css';
import {NotificationManager} from "react-notifications";
import {getErrorMsg} from "../../../assets/HelperFunctions/getErrorMsg";
import LoadingSpinner from "../../../components/LoadingSpinner/loadingSpinner";

class Food extends Component{
    constructor(props) {
        super(props);
        this.state={
            food:{},
            orderCount:1,
            loading: false,
        };
    }

    componentDidMount() {
        const fId = this.props.fId;
        const rId = this.props.rId;
        this.setState({loading:true}, ()=> {
            axios.get(`http://185.166.105.6:30008/restaurants/${rId}/foods/${fId}`,
                {headers: { Authorization: localStorage.getItem('jwt') }})
                .then((response) => {
                console.log(response);
                this.setState({
                    food: response.data,
                    loading: false,
                });
            }).catch((error) => {
                console.log(error);

                if( error.response && (error.response.data.error === "NO_JWT_TOKEN" || error.response.data.error === "INVALID_JWT_TOKEN")){
                    localStorage.removeItem('jwt');
                    this.props.history.push('/');
                }
                else {
                    const error_msg = getErrorMsg(error);
                    NotificationManager.error(null, error_msg, 3000);
                }
            })
        });
    }

    addFood(rID, fID, count, updateCart, event){
        event.preventDefault();
        axios.put(('http://185.166.105.6:30008/cart'), null,
            {
                params:{'restaurantId':rID, 'foodId':fID, 'count':count, 'foodParty':false },
                headers: { Authorization: localStorage.getItem('jwt') }
            })
            .then((response)=>{
            console.log(response);
            NotificationManager.success(null, 'غذا به سبد خرید اضافه شد');
            updateCart();
        }).catch((error)=>{
            console.log(error);

            if( error.response && (error.response.data.error === "NO_JWT_TOKEN" || error.response.data.error === "INVALID_JWT_TOKEN")){
                localStorage.removeItem('jwt');
                this.props.history.push('/');
            }
            else {
                const error_msg = getErrorMsg(error);
                NotificationManager.error(null, error_msg, 3000);
            }
        })
    }

    decreaseOrderCount(event){
        event.preventDefault();
        if(this.state.orderCount>1){
            this.setState({
                orderCount:this.state.orderCount-1,
            });
        }
    }

    increaseOrderCount(event){
        this.setState({
            orderCount:this.state.orderCount+1,
        });
    }

    render(){
        const rId = this.props.rId;
        const fId = this.props.fId;
        return(
            <React.Fragment>
                <div className="container-fluid">
                    {this.state.loading ? <LoadingSpinner/> :
                        <div>
                            <div className="light-text food-details-title"> {this.state.food.restaurantName} </div>
                            <div className="row no-radius dashed-border-bottom">
                                <div className="col-4 update-border">
                                    <img src={this.state.food.image} className="modal-food-image" alt="food_img"/> <br/>
                                </div>
                                <div className="col-8 row right-align food-data">
                                    <div className="col-12">
                                        <span className="bold-text food-title"> {this.state.food.name} </span>
                                        <span className="star food-title">&#9733;</span>
                                        <span className="light-text food-title"> {this.state.food.popularity} </span>
                                        <div className="col-12 food-description">{this.state.food.description}</div>
                                        <br/>
                                        <div className="light-text food-title">{this.state.food.price} تومان</div>
                                        <br/>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div className="row select-count">
                                    <div className="col-4"></div>
                                    <div className="col-3">
                                        <a className="not-link minus" href="#"
                                           onClick={this.decreaseOrderCount.bind(this)}>
                                            <span className="flaticon-minus remove-food"></span>
                                        </a>
                                        <span className="food-num">{this.state.orderCount}</span>
                                        <a className="not-link" href="#" onClick={this.increaseOrderCount.bind(this)}>
                                            <span className="flaticon-plus add-food"></span>
                                        </a>
                                    </div>
                                    <div className="col-5">
                                        <form
                                            onSubmit={this.addFood.bind(this, rId, fId, this.state.orderCount, this.props.updateCart)}>
                                            <input className="modal-add-cart align-left" type="submit"
                                                   value="اضافه کردن به سبد خرید"/>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    }
                </div>
            </React.Fragment>
        );
    }
}

export default Food;