import React, {Component} from 'react';
import axios from 'axios';

import MenuContainer from "./menuContainer";
import Cart from "../../../components/Cart/cart";
import {NotificationManager} from "react-notifications";
import {getErrorMsg} from "../../../assets/HelperFunctions/getErrorMsg";

class RestaurantDetailsContainer extends Component{
    constructor(props) {
        super(props);
        this.state = {
            cart:{items:[]},
            loading:false,
        };
        this.updateCart = this.updateCart.bind(this);
    };

    updateCart(){
        this.setState({loading:true}, ()=> {
            axios.get('http://185.166.105.6:30008/cart',
                {headers: { Authorization: localStorage.getItem('jwt') }})
                .then((response) => {
                console.log(response);
                this.setState({
                    cart: response.data,
                    loading:false,
                });
            }).catch((error) => {
                console.log(error);

                if( error.response && (error.response.data.error === "NO_JWT_TOKEN" || error.response.data.error === "INVALID_JWT_TOKEN")){
                    localStorage.removeItem('jwt');
                    this.props.history.push('/');
                }
                else {
                    const error_msg = getErrorMsg(error);
                    NotificationManager.error(null, error_msg, 3000);
                }
            })
        });
    }

    componentDidMount() {
       this.updateCart();
    }

    render() {
        return(
            <div className="container-fluid text-center content-wrap restaurant-page">
                <div className="row">
                    <div className="col-4"></div>
                    <div className="col-8 center-items">
                        <span className="bold-text navy-underlined-title">منوی غذا</span>
                    </div>
                </div>
                <div className="row">
                    <div className="list-group list-group-horizontal restaurant-pannel">
                        <div className="col-4 restaurant-section">
                            <section className="cart-section">
                                <Cart cart={this.state.cart} updateCart ={this.updateCart} loading={this.state.loading} {...this.props}/>
                            </section>
                        </div>
                        <div className="col-8 restaurant-section right-border">
                            <MenuContainer restaurantId={this.props.restaurantId} menu={this.props.menu} updateCart={this.updateCart} {...this.props}/>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
export default RestaurantDetailsContainer;