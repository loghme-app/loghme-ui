import React, {Component} from "react";
import {Modal} from "react-bootstrap";
import Food from "./Food";

class MenuContainer extends Component{
    constructor(props) {
        super(props);
        this.state={
            showFoodDetails: false,
            rId: "",
            fId: "",
        };
        this.closeFood = this.closeFood.bind(this);
    }

    openFood(rId, foodId, event){
        event.preventDefault();
        this.setState({
            showFoodDetails: true,
            rId: rId,
            fId: foodId,
        });
    }

    closeFood(){
        this.setState({
            showFoodDetails: false,
            rId: "",
            fId: "",
        });
    }

    render() {
        const menu = this.props.menu;
        const rID = this.props.restaurantId;
        return (
            <div className="container start-foods-section">
                <Modal dialogClassName="food-modal" show={this.state.showFoodDetails} onHide={this.closeFood}>
                    <Modal.Body >
                        <Food rId={this.state.rId} fId={this.state.fId} updateCart={this.props.updateCart} {...this.props}/>
                    </Modal.Body>
                </Modal>

                <div className="row  center-items">
                    {menu.map(item => (
                        <div key={item.id} className="co-4 food-container">
                            <img src={item.image} className="food-image" alt="food_img"/> <br/>

                            <span className="bold-text food-info">{item.name}</span>
                            <span className="light-text right-padding food-info">{item.popularity}</span>
                            <span className="star food-info">&#9733;</span>
                            <br/>
                            <span className="light-text food-info">{item.price} تومان</span> <br/>

                            <div className="container">
                                <div className="row">
                                    <div className="col-12 justify-content-center align-items-centercenter-items">
                                        <form onSubmit={this.openFood.bind(this, rID, item.id)}>
                                            <input type="submit" value="افزودن به سبد خرید"
                                                   className="add-to-cart-btn food-info"/>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    ))}
                </div>
            </div>
        );
    }
}

export default MenuContainer;