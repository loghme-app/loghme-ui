import React, {Component} from 'react';
import axios from 'axios';

import './restaurant.css';
import RestaurantInfoBar from "./components/restaurantInfoBar";
import RestaurantDetailsContainer from "./components/restaurantDetailsContainer";
import LoadingSpinner from "../../components/LoadingSpinner/loadingSpinner";
import {NotificationManager} from "react-notifications";
import {getErrorMsg} from "../../assets/HelperFunctions/getErrorMsg";

class Restaurant extends Component{
    constructor(props) {
        super(props);
        this.state= {
            restaurant: {menu: []},
            loading: false,
        };
    }

    componentDidMount() {
        const restaurantID = this.props.match.params.restaurantID;
        this.setState({loading:true}, ()=>{
            axios.get(`http://185.166.105.6:30008/restaurants/${restaurantID}`,
                {headers: { Authorization: localStorage.getItem('jwt') }})
                .then((response)=> {
                console.log(response);
                this.setState({
                    restaurant: response.data,
                    loading: false,
                });
            }).catch((error)=> {
                console.log(error);

                if( error.response && (error.response.data.error === "NO_JWT_TOKEN" || error.response.data.error === "INVALID_JWT_TOKEN")){
                    localStorage.removeItem('jwt');
                    this.props.history.push('/');
                }
                else {
                    const error_msg = getErrorMsg(error);
                    NotificationManager.error(null, error_msg, 3000);
                }
            })
        });
    }

    render() {
        return(
            <div className="content-wrap container-fluid">
                <div className="row simple-head-bar"/>
                {this.state.loading ? <LoadingSpinner/> :
                    <div>
                        <RestaurantInfoBar restaurantName={this.state.restaurant.name}
                                           restaurantLogo={this.state.restaurant.logo}/>

                        <RestaurantDetailsContainer menu={this.state.restaurant.menu}
                                                    restaurantId={this.state.restaurant.id} {...this.props}/>
                    </div>
                }
            </div>
        );
    }
}

export default Restaurant;